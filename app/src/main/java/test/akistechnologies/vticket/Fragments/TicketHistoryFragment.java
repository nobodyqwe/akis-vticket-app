package test.akistechnologies.vticket.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import test.akistechnologies.vticket.DrawerActivity;
import test.akistechnologies.vticket.R;
import test.akistechnologies.vticket.global.SessionManager;
import test.akistechnologies.vticket.global.WebClient;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TicketHistoryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TicketHistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TicketHistoryFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


    private OnFragmentInteractionListener mListener;

    private RecyclerView mTicketList;
    private JSONArray mTicketHistory;
    private LinearLayoutManager mLayoutManager;
    private TicketAdapter mTicketAdapter;
    Context mContext;
    SessionManager mSession;
    SwipeRefreshLayout mSwipeLayout;

    public TicketHistoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TicketHistoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TicketHistoryFragment newInstance() {
        TicketHistoryFragment fragment = new TicketHistoryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mTicketHistory = new JSONArray();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ticket_history, container, false);
        mTicketList = (RecyclerView) view.findViewById(R.id.ticket_list);
        mLayoutManager = new LinearLayoutManager(mContext);
        mTicketList.setLayoutManager(mLayoutManager);

        mTicketAdapter = new TicketAdapter();
        mTicketList.setAdapter(mTicketAdapter);

/*
        ImageView drawerImage = (ImageView) view.findViewById(R.id.drawerImage);
        drawerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.openDrawer();
            }
        });*/

        updateHeader(view, mListener, "History");



        mSwipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                LoadHistory();
            }
        });

        if (!mSession.isLoggedIn()){
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setCancelable(false);

            builder.setMessage(R.string.need_to_login);
            builder.setPositiveButton(getString(R.string.login),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mListener.changeFragment(DrawerActivity.mFragmentTypes.LoginFragment, null);
                        }
                    });
            builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mListener.changeFragment(DrawerActivity.mFragmentTypes.CompaniesFragment, null);
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }else{
            LoadHistory();
        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext  = context;
        mSession = new SessionManager(mContext);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public void LoadHistory(){
        try{
            mSwipeLayout.setRefreshing(true);
            JSONObject method = new JSONObject();
            method.put("method","ticketHistory");
            JSONObject params = new JSONObject();
            params.put("session_token",mSession.getValue("session_token"));
            method.put("params", params);

            WebClient.post(mContext, "post/history",method.toString(), new JsonHttpResponseHandler(){

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    Log.d("Get history", "onSuccess: "+response.toString());
                    try{
                        if (response.getJSONArray("results").length() > 0){
                            mTicketHistory = response.getJSONArray("results");
                            mTicketAdapter.notifyDataSetChanged();
                            mSwipeLayout.setRefreshing(false);
                        }
                    }catch (Exception e){
                        Log.d("Get history", "onSuccess: "+e.toString());
                    }

                }

            });

        }catch (Exception e){
            Log.d("Ticket history", "LoadHistory: "+e.toString());
        }
    }



    class TicketAdapter extends RecyclerView.Adapter<TicketAdapter.TicketViewHolder>{

        class TicketViewHolder extends RecyclerView.ViewHolder{

            private TextView ticket;
            private TextView wait_time;
            private TextView service_time;
            private TextView served_by;
            private int id = 0;
            public JSONObject mData;

            TicketViewHolder(View itemView) {
                super(itemView);
                ticket = (TextView) itemView.findViewById(R.id.ticket);
                served_by = (TextView) itemView.findViewById(R.id.served_by);
                wait_time = (TextView) itemView.findViewById(R.id.wait_time);
                service_time = (TextView) itemView.findViewById(R.id.serve_time);

            }


            public void setLayoutParams(){
                try{
                    ticket.setText(mData.getString("ticket_value"));
                    served_by.setText(getString(R.string.served_by, mData.getString("served_by")));
                    service_time.setText(getString(R.string.service_time, avg(mData.getInt("serving_time"))));
                    wait_time.setText(getString(R.string.wait_time, avg(mData.getInt("wait_time"))));
                    if (mData.getInt("canceled") == 1){
                        served_by.setVisibility(View.INVISIBLE);
                    }
                }catch (Exception e){
                    Log.d("QueueViewHolder", "setLayoutParams: "+e.toString());
                }
            }

            private String avg(int time){
                String result = "";
                int sec = 0;
                int min = 0;
                if (time < 60){
                    sec = time;
                }else{
                    sec = time % 60;
                }
                if (time >= 60){
                    min = Math.round(time/60);
                }

                if (min > 0){
                    result = getString(R.string.min, String.valueOf(min));
                }

                if (sec > 0){
                    result = result+" "+getString(R.string.sec, String.valueOf(sec));
                }

                if (result.equals("")){
                    result = "0 s";
                }


                return result;
            }
        }


        @Override
        public int getItemCount() {
            return mTicketHistory.length();
        }


        @Override
        public TicketViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.ticket_holder, viewGroup, false);
            TicketViewHolder pvh = new TicketViewHolder(v);
            return pvh;
        }

        @Override
        public void onBindViewHolder(TicketViewHolder ticketViewHolder, int i) {
            try{
                ticketViewHolder.mData = mTicketHistory.getJSONObject(i);
                ticketViewHolder.setLayoutParams();
            }catch (Exception e){
                Log.d("Queues recycler view", "onBindViewHolder: "+e.toString());
            }
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }
    }



}
