package test.akistechnologies.vticket.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/*
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
*/
import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import test.akistechnologies.vticket.DrawerActivity;
import test.akistechnologies.vticket.MainActivity;
import test.akistechnologies.vticket.R;
import test.akistechnologies.vticket.global.Select;
import test.akistechnologies.vticket.global.Ticket;
import test.akistechnologies.vticket.global.WebClient;
import test.akistechnologies.vticket.listeners.EndlessScrollListener;
import test.akistechnologies.vticket.listeners.OnLoadMoreListener;
import test.akistechnologies.vticket.utils.Helpers;

import static android.content.Context.LOCATION_SERVICE;
/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link LocationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LocationFragment extends BaseFragment  implements Spinner.OnItemSelectedListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String TAG = LocationFragment.class.getSimpleName();

    // TODO: Rename and change types of parameters
    private OnFragmentInteractionListener mListener;


    public Spinner dropdown;
    private ArrayList<String> data;
    private ArrayAdapter<String> dataAdapter;
    private JSONArray locationData;
    public ProgressDialog progress;
    private RecyclerView cards;
    private List<JSONObject> queueStatus;
    private Context mContext;

    private RecyclerView mLocationList;
    private LinearLayoutManager mLayoutManager;
    private LocationAdapter mLocationAdapter;
    private String mSearchTerm = "";
    SwipeRefreshLayout mSwipeLayout;

    private int skiped = 0;
    private int load = 10;

    public LocationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LocationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LocationFragment newInstance() {
        LocationFragment fragment = new LocationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        String token = FirebaseInstanceId.getInstance().getToken();
        RequestParams params = new RequestParams();
        params.add("token",token);

        WebClient.post("post/active", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    Log.d("sdf5sdf45sdf", "onSuccess: "+response.toString());
                        Log.d("ticket search", "onSuccess: "+" found");/*

                        fragment = new TicketFragment();
                        ((TicketFragment)fragment).transferData(response.getJSONObject("data"));
                        getSupportFragmentManager()
                                .beginTransaction()
                                .add(R.id.fragmentHolder, fragment, "LocationList")
                                .commit();*/
                    mListener.changeFragment(DrawerActivity.mFragmentTypes.TicketFragment, response.getJSONObject("data"));


                } catch (Exception e) {
                    Log.d("zxc", "onSuccess: " + e.toString());
                }
            }

            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_location, container, false);

        mContext = getActivity();

        TextView headerCompany = (TextView) view.findViewById(R.id.headerCompany);
        headerCompany.setText(Select.getInstance().mCompanyName);

        ImageView backCompany = (ImageView) view.findViewById(R.id.backCompany);
        backCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.changeFragment(DrawerActivity.mFragmentTypes.CompaniesFragment,null);
            }
        });
/*
        progress = new ProgressDialog(view.getContext());
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
*/
        mLocationList = (RecyclerView) view.findViewById(R.id.locationList);
        mLocationList.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(mContext);
        mLocationList.setLayoutManager(mLayoutManager);
        mLocationAdapter = new LocationAdapter();
        mLocationList.setAdapter(mLocationAdapter);

        locationData = new JSONArray();

        loadLocations();

        queueStatus = new ArrayList<>();

        setDrawer(view, mListener);

        final MaterialEditText mSearchText = (MaterialEditText) view.findViewById(R.id.searchText);

        mSearchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mSearchTerm = editable.toString();
                if (mSearchTerm.length() >= 3) {
                    Log.d("seach", "afterTextChanged: " + mSearchTerm);
                    loadLocations(mSearchTerm);
                }else{
                    loadLocations();
                }
            }
        });


        mSwipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeLayout.setRefreshing(true);
                loadLocations();
            }
        });

        mLocationList.setOnScrollListener(new EndlessScrollListener(mLayoutManager) {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                mSwipeLayout.setRefreshing(true);
                loadLocations(mSearchTerm, page);
                return true;
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentInteractionListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement OnFragmentInteractionListener");
        }
        Ticket.getInstance().setState(0);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void loadLocations() {
        RequestParams params = new RequestParams();
        params.add("id", String.valueOf(Select.getInstance().mCompanyId));
        String[] gps = mListener.getGps();
        params.add("latitude", gps[1]);
        params.add("longitude", gps[0]);

        WebClient.post("get/companyLocations", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    JSONArray object = response.getJSONArray("data");
                    locationData = object;
                   // data.add("");
                    /*
                    for (int i = 0; i < object.length(); i++) {
                        data.add(object.getJSONObject(i).getString("name"));
                    }
                    for (int i = 0; i <= locationData.length() - 1; i++) {
                        if (locationData.getJSONObject(i).getString("latitude") != "null") {
                            // Add a marker in Sydney and move the camera
                            //  LatLng marker = new LatLng(locationData.getJSONObject(i).getDouble("latitude"), locationData.getJSONObject(i).getDouble("longitude"));
                            //  mMap.addMarker(new MarkerOptions().position(marker).title(locationData.getJSONObject(i).getString("name")));
                        }
                    }*/
                    mLocationAdapter.notifyDataSetChanged();
                   // progress.dismiss();
                    mSwipeLayout.setRefreshing(false);
                } catch (Exception e) {
                    Log.d("zxc", "onSuccess: " + e.toString());
                }
            }

            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                // Pull out the first event on the public timeline
                //JSONObject firstEvent = timeline.get(0);
                //String tweetText = firstEvent.getString("text");


            }
        });
    }

    private void loadLocations(String name) {
        RequestParams params = new RequestParams();
        params.add("id", String.valueOf(Select.getInstance().mCompanyId));
        params.add("name", name);
        String[] gps = mListener.getGps();
        params.add("latitude", gps[1]);
        params.add("longitude", gps[0]);

        WebClient.post("get/companyLocations", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    JSONArray object = response.getJSONArray("data");
                    locationData = object;
                    mLocationAdapter.notifyDataSetChanged();
                    //progress.dismiss();
                    mSwipeLayout.setRefreshing(false);
                } catch (Exception e) {
                    Log.d("zxc", "onSuccess: " + e.toString());
                }
            }

            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {

            }
        });
    }

    private void loadLocations(String name, int page) {
        RequestParams params = new RequestParams();
        params.add("id", String.valueOf(Select.getInstance().mCompanyId));
        params.add("name", name);
        params.add("skip", String.valueOf(page));
        String[] gps = mListener.getGps();
        params.add("latitude", gps[1]);
        params.add("longitude", gps[0]);

        WebClient.post("get/companyLocations", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    JSONArray jsonArray = response.getJSONArray("data");

                    for (int i=0; i<jsonArray.length(); i++) {
                        locationData.put(jsonArray.get(i));
                    }

                    mLocationAdapter.notifyItemRangeInserted(mLocationAdapter.getItemCount(), locationData.length());
                    //progress.dismiss();
                    mSwipeLayout.setRefreshing(false);
                } catch (Exception e) {
                    Log.d("zxc", "onSuccess: " + e.toString());
                }
            }

            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {

            }
        });
    }

    private void getQueueStatus(int i){
        WebClient.get("get/queuestatus/"+i, null, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    queueStatus.clear();
                    JSONArray object = response.getJSONArray("data");
                    for(int z=0;z<=object.length()-1;z++){
                        queueStatus.add(object.getJSONObject(z));
                    }
                } catch (Exception e) {
                    Log.d("zxc", "onSuccess: " + e.toString());
                }
            }

            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                // Pull out the first event on the public timeline
                //JSONObject firstEvent = timeline.get(0);
                //String tweetText = firstEvent.getString("text");


            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item
       // Toast.makeText(parent.getContext(), "Selected: " + item + " " + data.indexOf(item), Toast.LENGTH_LONG).show();
        try {
          //  if (position != 0) {
                getQueueStatus(locationData.getJSONObject(data.indexOf(item)).getInt("id"));
           /* }else{
                queueStatus.clear();
                adapter.notifyDataSetChanged();
            }*/
            /*
            if (locationData.getJSONObject(data.indexOf(item)).getString("latitude") != "null") {
                // Add a marker in Sydney and move the camera
                LatLng sydney = new LatLng(locationData.getJSONObject(data.indexOf(item)).getDouble("latitude"), locationData.getJSONObject(data.indexOf(item)).getDouble("longitude"));
                mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Laisve"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            }
            */
        } catch (Exception e) {
            Log.d("zxc", "onItemSelected: " + e.toString());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    class LocationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

        class LocationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            private TextView name;
            private int id = 0;

            LocationViewHolder(View itemView) {
                super(itemView);
                name = (TextView) itemView.findViewById(R.id.name);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                try {
                   // Toast.makeText(mContext, "You clicked "+ name.getText(), Toast.LENGTH_SHORT).show();
                    Select.getInstance().SetLocation(id,name.getText().toString());
                    mListener.changeFragment(DrawerActivity.mFragmentTypes.QueueFragment,null);
                }catch (Exception e){
                    Log.d("Companies item view", "onBindViewHolder: "+e.toString());
                }
            }
        }


        @Override
        public int getItemCount() {
            return locationData.length();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.language_item, viewGroup, false);
            LocationAdapter.LocationViewHolder pvh = new LocationAdapter.LocationViewHolder(v);
            return pvh;
        }



        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int i) {
            try{
                    LocationViewHolder locationViewHolder = (LocationViewHolder) holder;
                    locationViewHolder.id = locationData.getJSONObject(i).getInt("id");
                    locationViewHolder.name.setText(locationData.getJSONObject(i).getString("name"));

            }catch (Exception e){
                Log.d("Companies recycler view", "onBindViewHolder: "+e.toString());
            }
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

    }

}