package test.akistechnologies.vticket.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import test.akistechnologies.vticket.DrawerActivity;
import test.akistechnologies.vticket.R;
import test.akistechnologies.vticket.global.Ticket;
import test.akistechnologies.vticket.global.WebClient;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TicketFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class TicketFragment extends BaseFragment {

    private OnFragmentInteractionListener mListener;
    private JSONObject ticketData;
    private Button cancel;
    private Context mContext;
    static String TAG = "Ticket Fragment";
    private Runnable task;
    private Handler handler;

    public TicketFragment() {
        // Required empty public constructor
    }

    public void transferData(JSONObject data){
        ticketData = data;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ticket, container, false);

        TextView header = (TextView) view.findViewById(R.id.header);
        TextView ticket_value = (TextView) view.findViewById(R.id.ticket_value);
        TextView position = (TextView) view.findViewById(R.id.position);
        TextView insert = (TextView) view.findViewById(R.id.registration);
        cancel = (Button) view.findViewById(R.id.buttonCancel);

        try {
            header.setText(ticketData.getString("operation"));
            ticket_value.setText(ticketData.getString("ticket_value"));
            position.setText(getString(R.string.position,ticketData.getString("position")));
            insert.setText(getString(R.string.insert_time, ticketData.getString("insert_time")));

            Ticket.getInstance().id = ticketData.getInt("id");

        }catch (Exception e){
            Log.d(TAG, "onCreateView: "+e.toString());
        }

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                   AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                   builder.setCancelable(true);
                   //builder.setTitle("Title");
                   builder.setMessage("Cancel the ticket");
                   builder.setPositiveButton("Confirm",
                           new DialogInterface.OnClickListener() {
                               @Override
                               public void onClick(DialogInterface dialog, int which) {
                                   try{
                                       RequestParams params = new RequestParams();
                                       params.add("id",ticketData.getString("id"));

                                       WebClient.post("post/cancel", params, new JsonHttpResponseHandler() {

                                           @Override
                                           public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                               // If the response is JSONObject instead of expected JSONArray
                                               try {
                                                   Log.d("sdf5sdf45sdf", "onSuccess: "+response.toString());
                                                    if (response.getString("status").equals("error")){
                                                        Toast.makeText(mContext, "Error", Toast.LENGTH_SHORT).show();
                                                    }else{
                                                        mListener.changeFragment(DrawerActivity.mFragmentTypes.CompaniesFragment,null);
                                                    }
                                               } catch (Exception e) {
                                                   Log.d("zxc", "onSuccess: " + e.toString());
                                               }
                                           }

                                           public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                                               // Pull out the first event on the public timeline
                                               //JSONObject firstEvent = timeline.get(0);
                                               //String tweetText = firstEvent.getString("text");


                                           }
                                       });

                                   }catch (Exception e){

                                   }
                           }
                           });
                   builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                       }
                   });

                   AlertDialog dialog = builder.create();
                   dialog.show();
            }
        });

        return view;
    }


    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        mContext = context;

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        task = new Runnable() {
            @Override
            public void run() {
                try{
                    RequestParams params = new RequestParams();
                    params.add("id",ticketData.getString("id"));

                    WebClient.post("post/status", params, new JsonHttpResponseHandler() {

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            // If the response is JSONObject instead of expected JSONArray
                            try {
                                Log.d("sdf5sdf45sdf", "onSuccess: "+response.toString());

                                response = response.getJSONObject("data");
                                if (response.has("served")){
                                    Toast.makeText(context, "Ticket Served", Toast.LENGTH_SHORT).show();
                                    mListener.changeFragment(DrawerActivity.mFragmentTypes.CompaniesFragment,null);
                                }
                                if (response.has("called") && !response.has("served")){
                                    Toast.makeText(context, "Ticket called", Toast.LENGTH_SHORT).show();
                                }
                                if (!response.has("served")){
                                    handler.postDelayed(task, 5000);
                                }
                            } catch (Exception e) {
                                Log.d("zxc", "onSuccess: " + e.toString());
                            }
                        }

                        public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                            // Pull out the first event on the public timeline
                            //JSONObject firstEvent = timeline.get(0);
                            //String tweetText = firstEvent.getString("text");


                        }
                    });

                }catch (Exception e){

                }
            }
        };

        handler = new Handler();
        //handler.postDelayed(task, 5000);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    /*
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void changeFragment(int id, JSONObject data);
    }*/
}
