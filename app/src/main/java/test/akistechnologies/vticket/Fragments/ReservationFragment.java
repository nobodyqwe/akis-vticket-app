package test.akistechnologies.vticket.Fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import test.akistechnologies.vticket.DrawerActivity;
import test.akistechnologies.vticket.R;
import test.akistechnologies.vticket.global.WebClient;
import test.akistechnologies.vticket.views.EventDecorator;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ReservationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReservationFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String TAG = "Reservation fragment";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Context mContext;
    private ArrayList<String> data;
    private ArrayAdapter<String> dataAdapter;
    private ArrayList<String> dataLocations;
    private ArrayAdapter<String> dataLocationsAdapter;
    public Spinner dropdown;
    public Spinner dropdownLocations;
    public ProgressDialog progress;
    private HashMap<String, JSONObject> companiesData;
    private HashMap<String, JSONObject> locationData;

    public ReservationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ReservationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ReservationFragment newInstance(String param1, String param2) {
        ReservationFragment fragment = new ReservationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reservation, container, false);



        data = new ArrayList<String>();
        dataAdapter = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_dropdown_item, data);


        dataLocations = new ArrayList<String>();
        dataLocationsAdapter = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_dropdown_item, dataLocations);

        locationData = new HashMap<>();
        companiesData = new HashMap<>();

        progress = new ProgressDialog(view.getContext());
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
        getCompanies();

        MaterialCalendarView calendarView = (MaterialCalendarView) view.findViewById(R.id.calendarView);
        HashSet<CalendarDay> days = new HashSet<>();
        CalendarDay day = CalendarDay.from(2017,9,16);
        days.add(day);
        EventDecorator decorator = new EventDecorator(Color.BLACK, days);
        calendarView.invalidateDecorators();
        calendarView.addDecorator(decorator);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    public void getCompanies(){
        WebClient.get("get/companies",null, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try{
                    dataAdapter.clear();
                    data.clear();
                    companiesData.clear();
                    JSONArray object = response.getJSONArray("data");
                    for (int i = 0; i < object.length(); i++) {
                        data.add(object.getJSONObject(i).getString("name"));
                        companiesData.put(object.getJSONObject(i).getString("name"),object.getJSONObject(i));
                    }

                    dataAdapter.notifyDataSetChanged();
                    progress.dismiss();
                }catch (Exception e){
                    Log.d(TAG, "onSuccess: "+e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                //super.onFailure(statusCode, headers, responseString, throwable);

                dataAdapter.notifyDataSetChanged();
                progress.dismiss();
            }
        });
    }

    private void getLocations(){
        WebClient.post("get/locations",null, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try{
                    dropdownLocations.setVisibility(View.VISIBLE);
                    Log.d(TAG, "onSuccess: visible");
                    dataLocationsAdapter.clear();
                    dataLocations.clear();
                    locationData.clear();
                    JSONArray object = response.getJSONArray("data");
                    for (int i = 0; i < object.length(); i++) {
                        dataLocations.add(object.getJSONObject(i).getString("name"));
                        locationData.put(object.getJSONObject(i).getString("name"),object.getJSONObject(i));
                    }

                    dataLocationsAdapter.notifyDataSetChanged();
                    progress.dismiss();
                }catch (Exception e){
                    Log.d(TAG, "onSuccess: "+e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                //super.onFailure(statusCode, headers, responseString, throwable);
                dataLocationsAdapter.notifyDataSetChanged();
                progress.dismiss();
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        progress.show();
        getLocations();

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
