package test.akistechnologies.vticket.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import cz.msebera.android.httpclient.Header;
import test.akistechnologies.vticket.DrawerActivity;
import test.akistechnologies.vticket.R;
import test.akistechnologies.vticket.global.Select;
import test.akistechnologies.vticket.global.SessionManager;
import test.akistechnologies.vticket.global.WebClient;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link QueueFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link QueueFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class QueueFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    Context mContext;
    JSONArray mQueueList;
    RecyclerView mQueuesList;
    LinearLayoutManager mLayoutManager;
    QueueAdapter mQueueAdapter;
    SessionManager mSession;
    ProgressDialog progress;
    SwipeRefreshLayout mSwipeLayout;

    private OnFragmentInteractionListener mListener;

    public QueueFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment QueueFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static QueueFragment newInstance(String param1, String param2) {
        QueueFragment fragment = new QueueFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_queue, container, false);
        mQueueList = new JSONArray();

        mQueuesList = (RecyclerView) view.findViewById(R.id.queueList);
        mQueuesList.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(mContext);
        mQueuesList.setLayoutManager(mLayoutManager);
        mQueueAdapter = new QueueAdapter();
        mQueuesList.setAdapter(mQueueAdapter);

        TextView headerCompany = (TextView) view.findViewById(R.id.headerCompany);
        TextView headerCity = (TextView) view.findViewById(R.id.headerCity);
        headerCompany.setText(Select.getInstance().mCompanyName);
        headerCity.setText(Select.getInstance().mLocationName);


        ImageView backCompany = (ImageView) view.findViewById(R.id.backCompany);
        ImageView backLocation = (ImageView) view.findViewById(R.id.backLoction);
        backCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //mListener.changeFragment(DrawerActivity.mFragmentTypes.CompaniesFragment,null);
                mListener.popFragment(DrawerActivity.mFragmentTypes.CompaniesFragment);
            }
        });
        backLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.changeFragment(DrawerActivity.mFragmentTypes.LocationFragment,null);
            }
        });


        mSwipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                GetQueues();
            }
        });
        GetQueues();



        setDrawer(view, mListener);

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        mContext = context;
        mSession = new SessionManager(mContext);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private void GetQueues(){
        mSwipeLayout.setRefreshing(true);
        //ShowProgress();
        RequestParams params = new RequestParams();
        params.add("id",String.valueOf(Select.getInstance().mLocationId));

        WebClient.post("post/queuestatus", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    Log.d("sdf5sdf45sdf", "onSuccess: "+response.toString());
                    mQueueList = new JSONObject(response.toString()).getJSONArray("data");
                    mQueueAdapter.notifyDataSetChanged();
                    //progress.dismiss();
                    mSwipeLayout.setRefreshing(false);
                } catch (Exception e) {
                    Log.d("zxc", "onSuccess: " + e.toString());
                }
            }

            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
            }
        });
    }

    private void RegisterTicket(int id){
        RequestParams params = new RequestParams();
        params.add("id",String.valueOf(id));
        params.add("token", FirebaseInstanceId.getInstance().getToken());
        if (mSession.isLoggedIn()){
            params.add("session_token",mSession.getValue("session_token"));
        }

        WebClient.post("post/registerticket", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    Log.d("sdf5sdf45sdf", "onSuccess: "+response.toString());
                    JSONObject mData = new JSONObject(response.toString()).getJSONObject("data");
                    mListener.changeFragment(DrawerActivity.mFragmentTypes.TicketFragment, mData);
                } catch (Exception e) {
                    Log.d("zxc", "onSuccess: " + e.toString());
                }
            }

            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
            }
        });
    }

    private void ShowProgress(){
        progress = new ProgressDialog(mContext);
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }



    class QueueAdapter extends RecyclerView.Adapter<QueueAdapter.QueueViewHolder>{

        class QueueViewHolder extends RecyclerView.ViewHolder{

            private TextView header;
            private TextView workplaces;
            private TextView ticket_served;
            private TextView ticket_count;
            private TextView avg_wait_time;
            private TextView avg_service_time;
            private Button ticket_button;
            private Button reservation_button;
            private int id = 0;
            public JSONObject mData;

            QueueViewHolder(View itemView) {
                super(itemView);
                header = (TextView) itemView.findViewById(R.id.header);
                workplaces = (TextView) itemView.findViewById(R.id.workplaces);
                ticket_served = (TextView) itemView.findViewById(R.id.tickets_served);
                ticket_count = (TextView) itemView.findViewById(R.id.ticket_count);
                avg_wait_time = (TextView) itemView.findViewById(R.id.average_wait_time);
                avg_service_time = (TextView) itemView.findViewById(R.id.average_service_time);
                ticket_button = (Button) itemView.findViewById(R.id.ticket_button);
                reservation_button = (Button) itemView.findViewById(R.id.reservation_button);


                ticket_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        builder.setCancelable(true);
                        builder.setTitle("");
                        builder.setMessage("Are you sure you want a ticket?");
                        builder.setPositiveButton("Confirm",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            RegisterTicket(mData.getInt("id"));
                                        }catch(Exception e) {

                                        }
                                    }
                                });
                        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                });
            }


            public void setLayoutParams(){
                try{
                    header.setText(mData.getString("name"));
                    workplaces.setText(getString(R.string.workplaces, mData.getString("workplaces")));
                    ticket_count.setText(getString(R.string.ticket_count, mData.getString("ticket_count")));
                    ticket_served.setText(getString(R.string.tickets_served, mData.getString("tickets_served")));
                    avg_service_time.setText(getString(R.string.average_service_time, avg(mData.getInt("average_service_time"))));
                    avg_wait_time.setText(getString(R.string.average_wait_time, avg(mData.getInt("average_wait_time"))));
                }catch (Exception e){
                    Log.d("QueueViewHolder", "setLayoutParams: "+e.toString());
                }
            }

            private String avg(int time){
                String result = "";
                int sec = 0;
                int min = 0;
                if (time < 60){
                    sec = time;
                }else{
                    sec = time % 60;
                }
                if (time >= 60){
                    min = Math.round(time/60);
                }

                if (min > 0){
                    result = getString(R.string.min, String.valueOf(min));
                }

                if (sec > 0){
                    result = result+" "+getString(R.string.sec, String.valueOf(sec));
                }

                if (result.equals("")){
                    result = "0 s";
                }


                return result;
            }
        }
        @Override
        public int getItemCount() {
            return mQueueList.length();
        }

        @Override
        public QueueViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.status_card, viewGroup, false);
            QueueViewHolder pvh = new QueueViewHolder(v);
            return pvh;
        }

        @Override
        public void onBindViewHolder(QueueViewHolder queueViewHolder, int i) {
            try{
                queueViewHolder.mData = mQueueList.getJSONObject(i);
                queueViewHolder.setLayoutParams();
            }catch (Exception e){
                Log.d("Queues recycler view", "onBindViewHolder: "+e.toString());
            }
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }
    }
}
