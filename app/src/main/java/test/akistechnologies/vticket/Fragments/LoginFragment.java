package test.akistechnologies.vticket.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import test.akistechnologies.vticket.R;
import test.akistechnologies.vticket.global.FbCallBack;
import test.akistechnologies.vticket.global.SessionManager;
import test.akistechnologies.vticket.global.WebClient;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LoginFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends BaseFragment {

    private OnFragmentInteractionListener mListener;
    CallbackManager callbackManager;
    LoginFragment fragment;
    String mString = "";
    Context mContext;
    SessionManager mSession;

    public LoginFragment() {
        // Required empty public constructor
        fragment = this;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        updateHeader(view, mListener, getString(R.string.login));

        LoginButton loginButton = (LoginButton) view.findViewById(R.id.login_button);
        List<String> perms = new ArrayList<>();
        perms.add("email");
        perms.add("public_profile");
        loginButton.setReadPermissions(perms);
        // If using in a fragment
        loginButton.setFragment(this);
        Log.d("FB token", "onCreateView: loged in:"+isLoggedIn());


        if (isLoggedIn()){
            ConstraintLayout loginLayout = (ConstraintLayout) view.findViewById(R.id.loginLayout);
            loginLayout.setVisibility(View.INVISIBLE);
        }

        loginButton.registerCallback(FbCallBack.getInstance().callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("LOGIN", "onSuccess: FB LOGIN");
                Log.d("FB", "onSuccess: user:"+ loginResult.getAccessToken().getUserId());
                Log.d("FB", "onSuccess: Token:"+ loginResult.getAccessToken().getToken());
                String id = loginResult.getAccessToken().getUserId();
                SendLoginData(loginResult.getAccessToken().getToken(), loginResult.getAccessToken().getExpires().toString());
            }

            @Override
            public void onCancel() {
                // App code
                Log.d("LOGIN", "onSuccess: FB CANCEL");
            }

            @Override
            public void onError(FacebookException exception) {
                Log.d("LOGIN", "onSuccess: FB ERROR");
                Log.d("FB", "onError: "+ exception.toString());
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
    }

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        mContext = context;
        mSession = new SessionManager(mContext);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    /*
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void changeFragment(int id, JSONObject data);
    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        Bundle bundle = data.getExtras();
        if (bundle != null) {
            for (String key : bundle.keySet()) {
                Object value = bundle.get(key);
                Log.d("Login fragment extr", String.format("%s %s (%s)", key,
                        value.toString(), value.getClass().getName()));
            }
        }

        FbCallBack.getInstance().callbackManager.onActivityResult(requestCode, resultCode, data);
        Log.i("Login fragment", "OnActivityResult... ");
    }

    private void SendLoginData(final String token, String expires){
        RequestParams params = new RequestParams();
        params.add("token",token);
        params.add("expires",expires);

        WebClient.post("post/fb", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    Boolean no_email = false;
                    if(response.has("error")){
                        if (response.getString("error").equals("No email")){
                            no_email = true;
                        }
                    }
                    if (response.has("response")){
                        mSession.setLogin(true);
                        mSession.addValue("session_token", response.getString("session_token"));
                        if (response.getString("response").equals("ok")){
                            if (no_email){
                                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                builder.setTitle("Email");

                                // Set up the input
                                final EditText input = new EditText(mContext);

                                input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                                builder.setView(input);

                                    // Set up the buttons
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        mString = input.getText().toString();
                                        SendUpdateEmail(mString, token);
                                    }
                                });
                                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });

                                builder.show();
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.d("zxc", "onSuccess: " + e.toString());
                }
            }

            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                // Pull out the first event on the public timeline
                //JSONObject firstEvent = timeline.get(0);
                //String tweetText = firstEvent.getString("text");
            }


        });
    }

    private void SendUpdateEmail(String email, String token){
        JSONObject method = new JSONObject();
        try {
            method.put("method","fb_email");
            JSONObject params = new JSONObject();
            params.put("email", email);
            params.put("token", token);
            method.put("params", params);
            Log.d("FB UPDATE EMAIL", "SendUpdateEmail: "+method.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        WebClient.post(mContext, "post/fbEmail", method.toString(), new JsonHttpResponseHandler(){

        });
    }

}
