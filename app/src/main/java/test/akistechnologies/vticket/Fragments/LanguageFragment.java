package test.akistechnologies.vticket.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

import test.akistechnologies.vticket.DrawerActivity;
import test.akistechnologies.vticket.R;
import test.akistechnologies.vticket.global.SessionManager;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LanguageFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LanguageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LanguageFragment extends BaseFragment {
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ArrayList<String> languages = new ArrayList<>();
    private RecyclerView mLanguageList;

    private OnFragmentInteractionListener mListener;
    private LinearLayoutManager mLayoutManager;
    private Context mContext;
    SessionManager mSession;

    public LanguageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LanguageFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LanguageFragment newInstance() {
        LanguageFragment fragment = new LanguageFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languages.add("Lietuviškai");
        languages.add("English");
        languages.add("Latviešų");
        languages.add("Ukraine");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_language, container, false);
        mLanguageList = (RecyclerView) view.findViewById(R.id.language_list);
        mLanguageList.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(mContext);
        mLanguageList.setLayoutManager(mLayoutManager);
        LanguageAdapter mLanguageAdapter = new LanguageAdapter();
        mLanguageList.setAdapter(mLanguageAdapter);

        setHeader(getString(R.string.choose_language));
        hideDrawer(view);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        mContext = context;
        mSession = new SessionManager(mContext);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    /*
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void changeFragment(int id, JSONObject data);
        void setLocale(String locale);
    }*/


     class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.LanguageViewHolder>{

        class LanguageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            private TextView name;
            private int id = 0;

            LanguageViewHolder(View itemView) {
                super(itemView);
                name = (TextView) itemView.findViewById(R.id.name);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "You clicked "+ languages.get(id), Toast.LENGTH_SHORT).show();
                switch (id){
                    case 0:
                        mListener.setLocale("lt");
                    break;
                    case 1:
                        mListener.setLocale("en");
                    break;
                    case 2:
                        mListener.setLocale("lv");
                    break;
                    case 3:
                        mListener.setLocale("ua");
                    break;
                }

            }
        }
        @Override
        public int getItemCount() {
            return languages.size();
        }

        @Override
        public LanguageViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.language_item, viewGroup, false);
            LanguageViewHolder pvh = new LanguageViewHolder(v);
            return pvh;
        }

        @Override
        public void onBindViewHolder(LanguageViewHolder languangeViewHolder, int i) {
         languangeViewHolder.name.setText(languages.get(i));
         languangeViewHolder.id = i;
        }

         @Override
         public void onAttachedToRecyclerView(RecyclerView recyclerView) {
             super.onAttachedToRecyclerView(recyclerView);
         }
    }















}
