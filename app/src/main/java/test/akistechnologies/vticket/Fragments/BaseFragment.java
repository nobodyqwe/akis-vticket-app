package test.akistechnologies.vticket.Fragments;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONObject;

import test.akistechnologies.vticket.DrawerActivity;
import test.akistechnologies.vticket.R;
import test.akistechnologies.vticket.global.Select;

/**
 * Created by eimantas on 2017-11-06.
 */

public class BaseFragment extends Fragment {

    String header = "";

    public void setHeader(String header) {
        this.header = header;
    }

    public void updateHeader(View view, final OnFragmentInteractionListener mListener, String header){

        ImageView drawerImage = (ImageView) view.findViewById(R.id.drawerImage);
        drawerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.openDrawer();
            }
        });

        TextView headerCompany = (TextView) view.findViewById(R.id.toolbarHeader);
        headerCompany.setText(header);
    }

    public void setDrawer(View view, final OnFragmentInteractionListener mListener){
        ImageView drawerImage = (ImageView) view.findViewById(R.id.drawerImage);
        drawerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.openDrawer();
            }
        });
    }

    public void hideDrawer(View view){
        ImageView drawerImage = (ImageView) view.findViewById(R.id.drawerImage);
        drawerImage.setVisibility(View.GONE);
        if (this.header != ""){

            TextView headerCompany = (TextView) view.findViewById(R.id.toolbarHeader);
            headerCompany.setText(header);
        }
    }

    public interface OnFragmentInteractionListener {
        void changeFragment(DrawerActivity.mFragmentTypes id, JSONObject data);
        void popFragment(DrawerActivity.mFragmentTypes id);
        void openDrawer();
        void setLocale(String locale);
        String[] getGps();
    }
}
