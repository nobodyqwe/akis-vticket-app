package test.akistechnologies.vticket.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import test.akistechnologies.vticket.DrawerActivity;
import test.akistechnologies.vticket.R;
import test.akistechnologies.vticket.global.Select;
import test.akistechnologies.vticket.global.WebClient;
import test.akistechnologies.vticket.listeners.EndlessScrollListener;
import test.akistechnologies.vticket.listeners.OnLoadMoreListener;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CompaniesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CompaniesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CompaniesFragment extends BaseFragment {

    private Toolbar test;
    private Context mContext;
    private JSONArray mCompanies;
    private RecyclerView mCompaniesList;
    private LinearLayoutManager mLayoutManager;
    private CompaniesAdapter mCompaniesAdapter;
    private EditText mSearchText;
    String mSearchTerm;

    private OnFragmentInteractionListener mListener;

    public CompaniesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment CompaniesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CompaniesFragment newInstance() {
        CompaniesFragment fragment = new CompaniesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCompanies = new JSONArray();
        GetCompanies();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_companies, container, false);;

        mCompaniesList = (RecyclerView) view.findViewById(R.id.companies_list);
        mCompaniesList.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(mContext);
        mCompaniesList.setLayoutManager(mLayoutManager);
        mCompaniesAdapter = new CompaniesAdapter();
        mCompaniesList.setAdapter(mCompaniesAdapter);

        mSearchText = (EditText) view.findViewById(R.id.searchText);

        mSearchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mSearchTerm = editable.toString();
                if (mSearchTerm.length() >= 3) {
                    Log.d("seach", "afterTextChanged: " + mSearchTerm);
                    GetCompanies(mSearchTerm);
                }else{
                    GetCompanies();
                }
            }
        });

        setDrawer(view,mListener);/*

        mCompaniesAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mCompanies.put(null);
                mCompaniesAdapter.notifyItemInserted(mCompanies.length()-1);

            }
        });*/
        mCompaniesList.setOnScrollListener(new EndlessScrollListener(mLayoutManager) {

            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                Log.d("COMPANIES", "onLoadMore: LOAD");

                GetCompanies(mSearchTerm, page);

                return true;
            }

        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        mContext = context;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    /*
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void changeFragment(int id, JSONObject data);
        void changeToolbar(int type);
        void openDrawer();
    }*/

    private void GetCompanies(){

        RequestParams params = new RequestParams();
        params.add("name","");

        WebClient.post("get/companies", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    Log.d("sdf5sdf45sdf", "onSuccess: "+response.toString());
                    mCompanies = new JSONObject(response.toString()).getJSONArray("data");
                    mCompaniesAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    Log.d("zxc", "onSuccess: " + e.toString());
                }
            }

            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
            }
        });
    }

    private void GetCompanies(String term){
        RequestParams params = new RequestParams();
        params.add("name",term);

        WebClient.post("get/companies", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    Log.d("sdf5sdf45sdf", "onSuccess: "+response.toString());
                    mCompanies = new JSONObject(response.toString()).getJSONArray("data");
                    mCompaniesAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    Log.d("zxc", "onSuccess: " + e.toString());
                }
            }

            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
            }
        });
    }

    private void GetCompanies(String term, int page){
        RequestParams params = new RequestParams();
        params.add("name",term);
        params.add("page", String.valueOf(page));

        WebClient.post("get/companies", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    Log.d("sdf5sdf45sdf", "onSuccess: "+response.toString());
                    JSONArray jsonArray = new JSONObject(response.toString()).getJSONArray("data");

                    for (int i=0; i<jsonArray.length(); i++) {
                        mCompanies.put(jsonArray.get(i));
                    }
                    mCompaniesAdapter.notifyItemRangeInserted(mCompaniesAdapter.getItemCount(), mCompanies.length());
                } catch (Exception e) {
                    Log.d("zxc", "onSuccess: " + e.toString());
                }
            }

            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
            }
        });
    }

    class CompaniesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{




        class CompaniesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            private TextView name;
            private int id = 0;

            CompaniesViewHolder(View itemView) {
                super(itemView);
                name = (TextView) itemView.findViewById(R.id.name);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                try {
                   // Toast.makeText(mContext, "You clicked "+ name.getText(), Toast.LENGTH_SHORT).show();
                    Select.getInstance().SetCompany(id, name.getText().toString());
                    mListener.changeFragment(DrawerActivity.mFragmentTypes.LocationFragment,null);
                }catch (Exception e){
                    Log.d("Companies item view", "onBindViewHolder: "+e.toString());
                }
            }
        }


        @Override
        public int getItemCount() {
            return mCompanies.length();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.language_item, viewGroup, false);
            CompaniesAdapter.CompaniesViewHolder pvh = new CompaniesAdapter.CompaniesViewHolder(v);
            return pvh;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int i) {
            try{

                    CompaniesViewHolder mCompany = (CompaniesViewHolder) holder;
                    mCompany.id = mCompanies.getJSONObject(i).getInt("id");
                    mCompany.name.setText(mCompanies.getJSONObject(i).getString("name"));

            }catch (Exception e){
                Log.d("Companies recycler view", "onBindViewHolder: "+e.toString());
            }
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

    }





}
