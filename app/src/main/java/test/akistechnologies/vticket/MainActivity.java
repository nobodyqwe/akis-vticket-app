package test.akistechnologies.vticket;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import test.akistechnologies.vticket.Fragments.LocationFragment;
import test.akistechnologies.vticket.Fragments.TicketFragment;
import test.akistechnologies.vticket.global.WebClient;
import test.akistechnologies.vticket.utils.NotificationUtils;

public class MainActivity extends AppCompatActivity  {

    public ProgressBar loading;
    private Fragment fragment;

    private NotificationUtils mNotificationUtils;
    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            Log.d("g4sg45sdg", "onReceive: "+"shit");
            if (bundle != null) {
                String string = bundle.getString("status");
                Log.d("g4sg45sdg", "onR2eceive: "+string);
                if (string.equals("served")){
                    Toast.makeText(context, "Ticket Served", Toast.LENGTH_SHORT).show();
                    //changeFragment(DrawerActivity.mFragmentTypes.CompaniesFragment, null);

                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("213123", "onCreate: ");
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d("wejgoegeg", "onCreate: "+token);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            mNotificationUtils = new NotificationUtils(this);
        }

        loading = (ProgressBar) findViewById(R.id.progressBar);
        loading.setVisibility(View.INVISIBLE);

        if (savedInstanceState == null) {
            RequestParams params = new RequestParams();
            params.add("token",token);

            WebClient.post("post/active", params, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    // If the response is JSONObject instead of expected JSONArray
                    try {
                        Log.d("sdf5sdf45sdf", "onSuccess: "+response.toString());
                        if (response.getString("status").equals("no ticket")){
                            Log.d("ticket search", "onSuccess: "+"not found");
                            getSupportFragmentManager()
                                    .beginTransaction()
                                    .add(R.id.fragmentHolder, LocationFragment.newInstance(), "LocationList")
                                    .commit();
                        }else{
                            Log.d("ticket search", "onSuccess: "+" found");
                            fragment = new TicketFragment();
                            ((TicketFragment)fragment).transferData(response.getJSONObject("data"));
                            getSupportFragmentManager()
                                    .beginTransaction()
                                    .add(R.id.fragmentHolder, fragment, "LocationList")
                                    .commit();
                        }
                    } catch (Exception e) {
                        Log.d("zxc", "onSuccess: " + e.toString());
                    }
                }

                public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                }
            });
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragmentHolder, LocationFragment.newInstance(), "LocationList")
                    .commit();
        }
        registerReceiver(receiver,new IntentFilter("test.akistechnologies.vticket"));

    }

/*
    @Override
    public void changeFragment(int id, JSONObject data) {
        if (id == 2){
            fragment = new TicketFragment();
            ((TicketFragment)fragment).transferData(data);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragmentHolder, fragment);
            ft.commit();
        }

        if (id == 1){
            fragment = new LocationFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragmentHolder, fragment);
            ft.commit();
        }
    }*/
}
