package test.akistechnologies.vticket;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import test.akistechnologies.vticket.global.Ticket;
import test.akistechnologies.vticket.utils.NotificationUtils;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by Nobody on 2017-09-20.
 */

public class MyAndroidFirebaseMsgService extends FirebaseMessagingService {
    private static final String TAG = "MyAndroidFCMService";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Log data to Log Cat
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        for(String currentKey : remoteMessage.getData().keySet()) {

            Log.d(TAG, "Notification Message data: " + currentKey + " "+ remoteMessage.getData().get(currentKey));
        }
        if (remoteMessage.getData().get("status").equals("called")){
            Ticket.getInstance().setState(1);
        }else{
            Ticket.getInstance().setState(2);
            Log.d(TAG, "onMessageReceived: served");
            sendDataToActivity();
        }

        //create notification
        createNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle());
    }

    private void createNotification( String messageBody, String messageTitle) {
        //Intent intent = new Intent( this , ResultActivity. class );
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
       // PendingIntent resultIntent = PendingIntent.getActivity( this , 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri notificationSoundURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
      //  NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder()
        NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder( this, NotificationUtils.ANDROID_CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel( true )
                .setSound(notificationSoundURI);
                //.setContentIntent(resultIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, mNotificationBuilder.build());
    }

    private void sendDataToActivity(){
        Intent intent = new Intent(NotificationUtils.ANDROID_CHANNEL_ID);
        intent.putExtra("status", "served");
        sendBroadcast(intent);
    }
}
