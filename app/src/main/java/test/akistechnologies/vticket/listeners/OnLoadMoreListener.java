package test.akistechnologies.vticket.listeners;

/**
 * Created by eimantas on 2017-11-08.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
