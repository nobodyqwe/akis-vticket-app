package test.akistechnologies.vticket.global;

import com.facebook.CallbackManager;

/**
 * Created by eimantas on 2017-10-11.
 */

public class FbCallBack {
    private static final FbCallBacks ourInstance = new FbCallBacks();

    public static FbCallBacks getInstance() {
        return ourInstance;
    }

    public static class FbCallBacks {
        public CallbackManager callbackManager;

        public void setManager(){
            callbackManager = CallbackManager.Factory.create();
        }
    }
}
