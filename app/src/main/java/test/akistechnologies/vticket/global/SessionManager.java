package test.akistechnologies.vticket.global;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

/**
 * Created by eimantas on 2017-11-06.
 */

public class SessionManager {

    private static String TAG = SessionManager.class.getSimpleName();

    // Shared Preferences
    private SharedPreferences pref;

    private Editor editor;
    private  Context _context;

    // Shared pref mode
    private int PRIVATE_MODE = Context.MODE_PRIVATE;

    // Shared preferences file name
    private static final String PREF_NAME = "AkisLogin";

    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn) {

        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);

        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }

    public void addValue(String key, String value){
        editor.putString(key, value);
        editor.commit();

        Log.d(TAG, "addValue: "+key+" key added");
    }

    public void addValue(String key, boolean value){
        editor.putBoolean(key, value);
        editor.commit();

        Log.d(TAG, "addValue: "+key+" key added");
    }

    public String getValue(String key){
        return pref.getString(key,"");
    }

    public boolean getBoolean(String key){
        return pref.getBoolean(key, false);
    }

    public void removeValue(String key){
        editor.remove(key);
        editor.commit();
    }

    public boolean hasValue(String key){
        return pref.contains(key);
    }

}
