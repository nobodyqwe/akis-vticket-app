package test.akistechnologies.vticket.global;

/**
 * Created by eimantas on 2017-09-24.
 */

public class Ticket {
    private static final TicketState ourInstance = new TicketState();

    public static TicketState getInstance() {
        return ourInstance;
    }

    public static class TicketState {
        public int state = 0;
        public int id = 0;


        public void setState(int id){
            state = id;
        }
    }
}
