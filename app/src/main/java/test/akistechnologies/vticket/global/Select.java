package test.akistechnologies.vticket.global;

/**
 * Created by eimantas on 2017-10-08.
 */

public class Select {
    private static final SelectedComapny ourInstance = new SelectedComapny();

    public static SelectedComapny getInstance() {
        return ourInstance;
    }

    public static class SelectedComapny {
        public int mCompanyId = 0;
        public String mCompanyName = "";
        public int mLocationId = 0;
        public String mLocationName = "";

        public void SetCompany(Integer id, String name){
            mCompanyId = id;
            mCompanyName = name;

        }

        public void SetLocation(int id, String name){
            mLocationId = id;
            mLocationName = name;
        }


    }
}
