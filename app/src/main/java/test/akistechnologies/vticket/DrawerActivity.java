package test.akistechnologies.vticket;

import android.*;
import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import test.akistechnologies.vticket.Fragments.BaseFragment;
import test.akistechnologies.vticket.Fragments.CompaniesFragment;
import test.akistechnologies.vticket.Fragments.LanguageFragment;
import test.akistechnologies.vticket.Fragments.LocationFragment;
import test.akistechnologies.vticket.Fragments.LoginFragment;
import test.akistechnologies.vticket.Fragments.QueueFragment;
import test.akistechnologies.vticket.Fragments.ReservationFragment;
import test.akistechnologies.vticket.Fragments.TicketFragment;
import test.akistechnologies.vticket.Fragments.TicketHistoryFragment;
import test.akistechnologies.vticket.global.FbCallBack;
import test.akistechnologies.vticket.global.SessionManager;
import test.akistechnologies.vticket.global.Ticket;
import test.akistechnologies.vticket.global.WebClient;
import test.akistechnologies.vticket.listeners.GPSTracker;
import test.akistechnologies.vticket.utils.NotificationUtils;

public class DrawerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        /*
        LocationFragment.OnFragmentInteractionListener,
        LanguageFragment.OnFragmentInteractionListener,
        TicketFragment.OnFragmentInteractionListener,
        CompaniesFragment.OnFragmentInteractionListener,
        LoginFragment.OnFragmentInteractionListener,
        TicketHistoryFragment.OnFragmentInteractionListener,
        QueueFragment.OnFragmentInteractionListener,
        */
        BaseFragment.OnFragmentInteractionListener {

    public static String TAG = DrawerActivity.class.getSimpleName();

    public static enum mFragmentTypes {
        LanguangeFragment,
        LocationFragment,
        CompaniesFragment,
        TicketFragment,
        ReservationFragment,
        LoginFragment,
        TicketHistoryFragment,
        QueueFragment
    };


    private Fragment fragment;
    SessionManager mSession;
    Locale myLocale;
    Context mContext;
    DrawerLayout drawer;
    GPSTracker mGps;
    private NotificationUtils mNotificationUtils;
    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            Log.d("g4sg45sdg", "onReceive: "+"shit");
            if (bundle != null) {
                String string = bundle.getString("status");
                Log.d("g4sg45sdg", "onR2eceive: "+string);
                if (string.equals("served")){
                    Log.d("zxcxzc", "onReceive: Served");
                    Toast.makeText(context, "Ticket Served", Toast.LENGTH_SHORT).show();
                    if (fragment instanceof  TicketFragment) {
                        changeFragment(mFragmentTypes.CompaniesFragment, null);
                    }

                }
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
        Log.d(TAG, "onPause: PAUSE ACTIVITY");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: RESUME ACTIVITY");
        registerReceiver(receiver, new IntentFilter(NotificationUtils.ANDROID_CHANNEL_ID));
        if (fragment instanceof  TicketFragment) {
            checkTicket();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        FbCallBack.getInstance().setManager();
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mContext = this;

        mSession = new SessionManager(this);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Log.d("213123", "onCreate: ");
        //String token = FirebaseInstanceId.getInstance().getToken();
       //Log.d("wejgoegeg", "onCreate: "+token);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            mNotificationUtils = new NotificationUtils(this);
        }



        if (!mSession.hasValue("gps")){
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(mContext);
            }
            builder.setMessage("Do you want to get near locations")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if ( ContextCompat.checkSelfPermission( mContext, Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
                                ActivityCompat.requestPermissions( (DrawerActivity)mContext, new String[] {  Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION  }, GPSTracker.ACCESS_COARSE_LOCATION);
                                mSession.addValue("gps",true);
                                Log.d(TAG, "onClick: GPS ENABLED");
                                mGps = new GPSTracker(mContext);
                            }else{
                                mSession.addValue("gps",false);
                            }
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            mSession.addValue("gps",false);
                            Log.d(TAG, "onClick: GPS DISABLED");
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .show();
        }else{
            mGps = new GPSTracker(mContext);
            Log.d(TAG, "onCreate: latitude "+mGps.getLatitude()+" longitude "+mGps.getLongitude());
        }



        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


        if (savedInstanceState == null) {
            RequestParams params = new RequestParams();
            params.add("session_token", mSession.getValue("session_token"));

            WebClient.post("post/session",params, new JsonHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try{
                        Log.d(TAG, "onSuccess: "+response.toString());
                        if (response.getString("response").equals("bad")){
                            mSession.setLogin(false);
                            mSession.removeValue("session_token");
                            AlertDialog.Builder builder;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Dialog_Alert);
                            } else {
                                builder = new AlertDialog.Builder(mContext);
                            }
                            builder.setMessage("Session expired do you want to login?")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with delete
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }
                    }catch (Exception e){
                        Log.d(TAG, "onSuccess: "+e.toString());
                    }
                }
            });

            if (!mSession.hasValue("lang")) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragmentHolder, LanguageFragment.newInstance(), mFragmentTypes.LanguangeFragment.toString())
                        .commit();
            }else{
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragmentHolder, CompaniesFragment.newInstance(), mFragmentTypes.CompaniesFragment.toString())
                        .commit();
            }


            registerReceiver(receiver,new IntentFilter(NotificationUtils.ANDROID_CHANNEL_ID));
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection Simp

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_gallery) {
            changeFragment(mFragmentTypes.CompaniesFragment,null);
        } else if (id == R.id.nav_slideshow) {
            changeFragment(mFragmentTypes.ReservationFragment,null);
        } else if (id == R.id.nav_manage) {
            changeFragment(mFragmentTypes.LoginFragment,null);
        } else if (id == R.id.ticket_history){
            changeFragment(mFragmentTypes.TicketHistoryFragment,null);
        } else if (id == R.id.gps){
            if ( ContextCompat.checkSelfPermission( mContext, Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
                ActivityCompat.requestPermissions( (DrawerActivity)mContext, new String[] {  Manifest.permission.ACCESS_COARSE_LOCATION  }, GPSTracker.ACCESS_COARSE_LOCATION);
            }
            if ( ContextCompat.checkSelfPermission( mContext, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
                ActivityCompat.requestPermissions( (DrawerActivity)mContext, new String[] {   Manifest.permission.ACCESS_FINE_LOCATION  }, GPSTracker.ACCESS_FINE_LOCATION);
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.END);
        return true;
    }

    @Override
    public void changeFragment(mFragmentTypes id, JSONObject data) {

        if (id == mFragmentTypes.LanguangeFragment){
            fragment = new LanguageFragment();
        }
        if (id == mFragmentTypes.LocationFragment){
            fragment = new LocationFragment();
        }
        if (id == mFragmentTypes.TicketFragment){
            fragment = new TicketFragment();
            ((TicketFragment)fragment).transferData(data);
        }
        if (id == mFragmentTypes.ReservationFragment){
            fragment = new ReservationFragment();
        }
        if (id == mFragmentTypes.CompaniesFragment){
            fragment = new CompaniesFragment();
        }
        if (id == mFragmentTypes.QueueFragment){
            fragment = new QueueFragment();
        }
        if (id == mFragmentTypes.LoginFragment){
            fragment = new LoginFragment();
        }
        if (id == mFragmentTypes.TicketHistoryFragment){
            fragment = new TicketHistoryFragment();
        }

        if (id != mFragmentTypes.LanguangeFragment){
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }


        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        ft.setReorderingAllowed(true);
        ft.replace(R.id.fragmentHolder, fragment);
        ft.addToBackStack(id.toString());
        ft.commit();
    }

    @Override
    public void popFragment(mFragmentTypes id) {
        getSupportFragmentManager().popBackStackImmediate(id.toString(), 0);
        changeFragment(id, null);
    }

    @Override
    public void setLocale(String lang) {

        myLocale = new Locale(lang);

        Resources res = getResources();

        DisplayMetrics dm = res.getDisplayMetrics();

        Configuration conf = res.getConfiguration();

        conf.locale = myLocale;

        res.updateConfiguration(conf, dm);

        Log.d("SetLocale", "setLocale: "+lang);
        mSession.addValue("lang", lang);
        changeFragment(mFragmentTypes.CompaniesFragment,null);

    }

    @Override
    public String[] getGps() {
        String[] gps = new String[] { "0", "0"};
        if (mGps.canGetLocation()){
            gps = new String[] { String.valueOf(mGps.getLongitude()), String.valueOf(mGps.getLatitude())};
        }
        return gps;
    }

    @Override
    public void openDrawer(){
        drawer.openDrawer(Gravity.RIGHT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        FbCallBack.getInstance().callbackManager.onActivityResult(requestCode, resultCode, data);
        Log.d("Drawer", "onActivityResult: ");
    }

    private void checkTicket(){
        try{
            RequestParams params = new RequestParams();
            params.add("id", String.valueOf(Ticket.getInstance().id));

            WebClient.post("post/status", params, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    // If the response is JSONObject instead of expected JSONArray
                    try {
                        Log.d(TAG, "onSuccess: "+response.toString());

                        response = response.getJSONObject("data");
                        if (response.has("served")){
                            Toast.makeText(mContext, "Ticket Served", Toast.LENGTH_SHORT).show();
                            changeFragment(DrawerActivity.mFragmentTypes.CompaniesFragment,null);
                        }

                    } catch (Exception e) {
                        Log.d(TAG, "onSuccess: " + e.toString());
                    }
                }

                public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                    // Pull out the first event on the public timeline
                    //JSONObject firstEvent = timeline.get(0);
                    //String tweetText = firstEvent.getString("text");


                }
            });

        }catch (Exception e){

        }
    }

}
