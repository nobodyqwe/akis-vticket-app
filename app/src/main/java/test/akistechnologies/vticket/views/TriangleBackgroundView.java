package test.akistechnologies.vticket.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import test.akistechnologies.vticket.R;

/**
 * Created by eimantas on 2017-09-27.
 */

public class TriangleBackgroundView extends View {
    Paint paint;
    Paint bgPaint;
    int back = 0;

    public TriangleBackgroundView(Context context) {
        super(context);
        init();
    }

    public TriangleBackgroundView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.TriangleBackgroundView, 0, 0);
        try {
            back = ta.getInt(R.styleable.TriangleBackgroundView_backgroundColor,0);
            Log.d("triangle view", "TriangleBackgroundView: "+back);
        } finally {
            ta.recycle();
        }

    }

    public TriangleBackgroundView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        paint = new Paint();
        paint.setStrokeWidth(1);
        paint.setAntiAlias(true);
        paint.setStrokeCap(Paint.Cap.SQUARE);
        paint.setStyle(Paint.Style.FILL);

        int bg = Color.parseColor("#00000000");

        if (back == 0) {
            paint.setColor(getResources().getColor(R.color.loadingBackgroundColor));
        }else{
            paint.setColor(getResources().getColor(R.color.toolbar2layer));
        }

        bgPaint= new Paint();
        bgPaint.setStyle(Paint.Style.FILL);
        bgPaint.setColor(bg);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (back == 0) {
            paint.setColor(getResources().getColor(R.color.loadingBackgroundColor));
        }else if (back == 1){
            paint.setColor(getResources().getColor(R.color.toolbar2layer));
        }else{
            paint.setColor(getResources().getColor(R.color.toolbar3layer));
        }


        int h = getMeasuredHeight();
        int w = getMeasuredWidth();

        canvas.drawRect(0,0,w,h,bgPaint);

        Path path = new Path();
        path.moveTo(0, h);
        path.lineTo(w, h);
        path.lineTo(0, 0);
        path.lineTo(0, h);
        path.close();

        canvas.drawPath(path,paint);
    }
}
