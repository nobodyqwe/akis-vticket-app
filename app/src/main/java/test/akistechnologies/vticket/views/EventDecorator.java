package test.akistechnologies.vticket.views;

import android.util.Log;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;

import java.util.Collection;
import java.util.HashSet;

/**
 * Created by eimantas on 2017-10-10.
 */

public class EventDecorator implements DayViewDecorator {

    private final int color;
    private final HashSet<CalendarDay> dates;

    public EventDecorator(int color, Collection<CalendarDay> dates) {
        this.color = color;
        this.dates = new HashSet<>(dates);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        Boolean should = dates.contains(day);
        if (should){
            Log.d("Event decorator", "shouldDecorate:"+ should+" "+day.getDate().toString());
        }
        return should;
    }

    @Override
    public void decorate(DayViewFacade view) {
        Log.d("asdasd", "decorate: decor");
        view.addSpan(new DotSpan(50, color));
    }
}
